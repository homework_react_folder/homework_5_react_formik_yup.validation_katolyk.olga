import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from "react-redux"; // компонент враппер, який прокине дані всьому застосунку чи дочірнім компонентам 
import App from './App.jsx';
import './index.scss';
import store from "./store"; // там в папці store головний файл index, тому в маршрут його можна не дописувати
// store - це наш глобальний об'єкт с даними 

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Provider store={store}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </Provider>
  </React.StrictMode>,
);
