import React from "react";
import PropTypes from "prop-types";
import "../ModalBase/ModalBase.scss";
import "./ModalDeleteFromBasket.scss";
import ModalWrapper from "../ModalBase/ModalWrapper.jsx";
import ModalBox from "../ModalBase/ModalBox.jsx";
import ModalClose from "../ModalBase/ModalClose.jsx";
import ModalHeader from "../ModalBase/ModalHeader.jsx"
import ModalBody from "../ModalBase/ModalBody.jsx";
import ModalFooter from "../ModalBase/ModalFooter.jsx";

const ModalDeleteFromBasket = ({ close, product, handleRemoveFromBasket }) => {
    // console.log("product in ModalDeleteFromBasket", product)

    const { imgCard, nameCard, article } = product;    

    return (
        <ModalWrapper onClick={close}>
            <ModalBox className="modal-second">
                <ModalClose onClick={close} />
                <img src={imgCard} className="modal-header__img" />
                <ModalHeader>
                    Product Delete!
                </ModalHeader>
                <ModalBody>By clicking the “Yes, Delete”, <br /> {nameCard} will be deleted.</ModalBody>
                <ModalFooter 
                    firstText="NO, CANCEL" 
                    secondaryText="YES, DELETE" 
                    firstClick={close} 
                    secondaryClick={() => {
                        handleRemoveFromBasket(article);   
                    }} 
                    />
            </ModalBox>
        </ModalWrapper>
    )
};

ModalDeleteFromBasket.propTypes = {
    close: PropTypes.func,
    product: PropTypes.object,
    handleRemoveFromBasket: PropTypes.func
};

export default ModalDeleteFromBasket;
