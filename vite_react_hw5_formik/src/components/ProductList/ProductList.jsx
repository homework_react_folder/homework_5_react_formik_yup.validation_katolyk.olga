import React from "react";
import PropTypes from "prop-types";
import ProductCard from "../ProductCard/ProductCard.jsx";
import "./ProductList.scss";

const ProductList = ({ products, handleImgZoomModal }) => {
    return (
        <div className="product-list">
            {products.map(product => (
                <ProductCard 
                    key={product.article} 
                    product={product} 
                    handleImgZoomModal={handleImgZoomModal}
                />
            ))}
        </div>
    );
};

export default ProductList; 

ProductList.propTypes = {
    products: PropTypes.array,
    handleImgZoomModal: PropTypes.func,
};

// const ProductList = ({ products, handleImgZoomModal, handleFavoritedList, handleAddBasketModal, setModalContent, handleModalDeleteFromBasket, handleRemoveFromBasket, isBasketPage }) => {
//     return (
//         <div className="product-list">
//             {products.map(product => (
//                 <ProductCard 
//                     key={product.article} 
//                     product={product} 
//                     handleImgZoomModal={handleImgZoomModal} 
//                     handleFavoritedList={handleFavoritedList}
//                     handleAddBasketModal={handleAddBasketModal} 
//                     setModalContent={setModalContent}
//                     handleRemoveFromBasket ={handleRemoveFromBasket}
//                     handleModalDeleteFromBasket ={handleModalDeleteFromBasket}
//                     isBasketPage={isBasketPage}
//                 />
//             ))}
//         </div>
//     );
// };

// ProductList.propTypes = {
//     products: PropTypes.array,
//     handleImgZoomModal: PropTypes.func,
//     handleFavoritedList: PropTypes.func,
//     handleAddBasketModal: PropTypes.func,
//     setModalContent: PropTypes.func,
//     handleRemoveFromBasket: PropTypes.func,
//     handleModalDeleteFromBasket: PropTypes.func,
//     isBasketPage: PropTypes.bool
// };
