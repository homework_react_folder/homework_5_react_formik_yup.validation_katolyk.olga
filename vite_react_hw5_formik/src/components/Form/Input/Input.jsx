import React from "react";
import { Field, ErrorMessage } from "formik";
import cn from "classnames";
import PropTypes from "prop-types";
import InputMask from "react-input-mask";
import "./Input.scss";

export const Input = (props) => {
    const {
        className,
        type,
        placeholder,
        name,
        label,
        error,
        mask,
        options,
        ...restProps
    } = props;

    return (
        <>
            <label className={cn("form-item", className, { "has-validation": error })}>
                <div className="input-container">
                    <p className="form-label">{label}</p>
                    {type === "select" ? (
                        <Field as="select" className="select-placeholder" name={name} {...restProps}>
                            <option value="" disabled>{placeholder}</option>
                            {options && options.map((option, index) => (
                                <option key={index} value={option.value}>
                                    {option.label}
                                </option>
                            ))}
                        </Field>
                    ) : mask ? (
                        <Field name={name}>
                            {({ field }) => (
                                <InputMask {...field} mask={mask} placeholder={placeholder}>
                                    {(inputProps) => <input {...inputProps} type={type} />}
                                </InputMask>
                            )}
                        </Field>
                    ) : (
                        <Field
                            type={type}
                            name={name}
                            placeholder={placeholder}
                            {...restProps}
                        />
                    )}
                </div>
                <ErrorMessage name={name} className="error-message" component={"p"} />
            </label>
        </>
    )
};

Input.defaultProps = {
    type: "text",
};

Input.propTypes = {
    className: PropTypes.string,
    type: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    error: PropTypes.bool,
    mask: PropTypes.string,
    restProps: PropTypes.object,
};
