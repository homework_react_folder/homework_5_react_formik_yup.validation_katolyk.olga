import React from "react";
import PropTypes from "prop-types";
import "../ModalBase/ModalBase.scss";
import "./ModalImgZoom.scss";
import ModalWrapper from "../ModalBase/ModalWrapper.jsx";
import ModalBox from "../ModalBase/ModalBox.jsx";
import ModalClose from "../ModalBase/ModalClose.jsx";
import ModalHeader from "../ModalBase/ModalHeader.jsx";

const ModalImgZoom = ({close, imgCard, nameCard }) => {
    
    return (
        <ModalWrapper onClick={close}>
            <ModalBox>
                <ModalClose onClick={close}/>
                <img src={imgCard} className="modal-header__img-zoom" alt={nameCard} />
                <ModalHeader>{nameCard}</ModalHeader>
            </ModalBox>
        </ModalWrapper>
    )
};

ModalImgZoom.propTypes = {
    close: PropTypes.func.isRequired,
    imgCard: PropTypes.string,
    nameCard: PropTypes.string
};

export default ModalImgZoom;
