import React from "react";
import PropTypes from "prop-types";
import cn from "classnames";

const ModalBox = ({children, className}) => {
    return (        
        <div className={cn("modal-box", className)}>
            {children}
        </div>   
    )
};

ModalBox.propTypes = {
    children: PropTypes.any,
    className: PropTypes.string
};

export default ModalBox;