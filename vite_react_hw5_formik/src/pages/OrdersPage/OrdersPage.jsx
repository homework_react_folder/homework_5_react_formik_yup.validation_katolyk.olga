import React from "react";
import { useSelector } from "react-redux";
import { selectorOrders } from "../../store/selectors.js";
import "./OrdersPage.scss";

const OrdersPage = () => {
    const orders = useSelector(selectorOrders);
    // const orderNumber = Date.now(); // Отримуємо поточну дату в мілісекундах для формування номера замовлення

    return (
        <>
            <div className="page__title">
                <h2 className="page__title-text">
                    Congratulations on a successful purchase.
                </h2>
            </div>
            <ul className="orders__title orders__container">Your order's list:
                {orders && orders.length > 0 ? (
                        orders.map((order, index) => (
                            <li key={index} className="orders__item">
                                <div className="orders__item--container">
                                    <p>Number of order: {order.numberOrder}</p>
                                    <p><span>Total of products: </span>{order.totalProductsInBasket}pcs.</p>
                                    <p><span>Amount in cart: </span>{order.amountInCart}$</p>
                                </div>
                                <div className="orders__item--container">
                                    <p><span>Name: </span>{order.name}</p>
                                    <p><span>Phone: </span>{order.phone}</p>
                                    <p><span>Email: </span>{order.email}</p>
                                    <p><span>City: </span>{order.city}</p>
                                    <p><span>Delivery: </span>{order.delivery}</p>
                                    <p><span>Comments: </span>{order.comments}</p>
                                </div>
                                <ul className="orders__item--container">
                                    {order.productsInOrder.map((product, productIndex) => (
                                        <li key={productIndex} className="orders__item--product">                                            
                                            <p><span>Name: </span>{product.nameCard}</p>
                                            <p><span>Price: </span>{product.price}$</p>
                                            <p><span>Article: </span>{product.article}</p>
                                            <p><span>Color: </span>{product.color}</p>
                                            <p><span>countInBasket: </span>{product.countInBasket}</p>                                             
                                        </li>
                                    ))}
                                </ul>
                            </li>
                        ))
                        ) : (
                        <li>No orders available.</li>
                    )}
            </ul >
        </>
    )
}

export default OrdersPage;




{`Congratulations on a successful purchase. 
// Your order number: 
// $/{orderNumber}`
}