import React from "react";
import './HomePage.scss';
import PropTypes from "prop-types";
import ProductList from "../../components/ProductList/ProductList";

const HomePage = ({ products, handleImgZoomModal }) => {
  return (
      <ProductList
        products={products}
        handleImgZoomModal={handleImgZoomModal}
      />
  )
}

HomePage.propTypes = {
  products: PropTypes.array,
  handleImgZoomModal: PropTypes.func,
};

export default HomePage;


// const HomePage = ({ products, handleImgZoomModal, handleFavoritedList, handleAddBasketModal, setModalContent }) => {
//     return (
//         <ProductList
//           products={products}
//           handleImgZoomModal={handleImgZoomModal}
//           handleFavoritedList={handleFavoritedList}
//           handleAddBasketModal={handleAddBasketModal}
//           setModalContent={setModalContent}
//         />
//     )
// }